##TODO LIST中启动及涉及技术资料

###1.启动
进入主目录,通过  *node server.js*  命令,启动web服务。   
用浏览器打开index.html。

###2.资料

####BootStrap:å
下载：[http://getbootstrap.com/2.3.2/](http://getbootstrap.com/2.3.2/)	
菜鸟教程：[http://www.runoob.com/bootstrap/bootstrap-tutorial.html](http://www.runoob.com/bootstrap/bootstrap-tutorial.html) 	
各种CSS样式库：[http://www.bootcss.com/](http://www.bootcss.com/)	

####Angular-js：
菜鸟教程：[http://www.runoob.com/angularjs/angularjs-tutorial.html](http://www.runoob.com/angularjs/angularjs-tutorial.html)

####React:
React 入门实例教程：[http://www.ruanyifeng.com/blog/2015/03/react.html](http://www.ruanyifeng.com/blog/2015/03/react.html)	
配套react-demos ：[https://github.com/ruanyf/react-demos](https://github.com/ruanyf/react-demos)		
ReactJs入门教程-精华版：[http://www.cnblogs.com/Leo_wl/p/4489197.html](http://www.cnblogs.com/Leo_wl/p/4489197.html)	
material-ui遇到这个问题，请问应该怎么处理？:[http://react-china.org/t/material-ui/6494](http://react-china.org/t/material-ui/6494)

####gulp：
新手教程：[http://www.tuicool.com/articles/FJVNZf](http://www.tuicool.com/articles/FJVNZf)