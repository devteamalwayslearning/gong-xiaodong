/**
 * Created by dev-002 on 16/10/17.
 */
var PORT = 3000;

var http = require('http');
var fs = require('fs');
var url = require('url');


//JS,判断是否已某字符串结尾
String.prototype.endWith=function(endStr){
    var d=this.length-endStr.length;
    return (d>=0&&this.lastIndexOf(endStr)==d)
}

var server = http.createServer(function (request, response) {
// 解析请求，包括文件名
    var pathname = url.parse(request.url).pathname;

    // 输出请求的文件名
    console.log("Request for " + pathname + " received.");

    // 从文件系统中读取请求的文件内容
    fs.readFile(pathname.substr(1), function (err, data) {
        if (err) {
            console.log(err);
            // HTTP 状态码: 404 : NOT FOUND
            // Content Type: text/plain
            response.writeHead(404, {'Content-Type': 'text/html'});
        }else{
            // 响应文件内容
            response.write(data.toString());
        }
        //  发送响应数据
        response.end();
    });
});
server.listen(PORT);
console.log("Server runing at port: " + PORT + ".");
