/**
 * Created by dev-002 on 16/10/17.
 */


(function (){
    //初始化App
    var app = angular.module('toDoList', []);
    //var arrayList = [{'id':'0','title':'天气不错','content':'今天天气确实不错','type':'工作'}];

    var arrayList;

    //判断是否支持
    var isSupport = window.localStorage ? true: false;

    //保存到Storage
    var saveToStorage = function(){
        if (isSupport){
            //storage.setItem("details",JSON.stringify(details));
            window.localStorage.setItem("todolist",JSON.stringify(arrayList));
        }
    };

    //从Stroage获取
    var fetchFromStorage = function (){
        if (isSupport){
            arrayList = JSON.parse(window.localStorage.getItem("todolist"));
        }
        if (!arrayList){
            arrayList = new Array();
        }
    };
    fetchFromStorage();

    app.controller('toDoCtrl', function($scope) {
        $scope.list = arrayList;
        $scope.modelTitle = '';
        $scope.modelContent = '';

        //添加所有类型
        $scope.types = ['工作','学习','生活','聚会','旅行'];

        //设置默认选项
        $scope.selectedType = '工作';

        //保存
        $scope.save = function(){
            if($scope.modelTitle == ""){
                $("#myModal").modal("show");
                return;
            }

            var t1 = {
                'id': (arrayList.length > 0)?(parseInt(arrayList[arrayList.length-1].id)+1+""):("0"),
                'title':$scope.modelTitle,
                'content':$scope.modelContent,
                'type':$scope.selectedType
            };
            arrayList.push(t1);
            saveToStorage();
        }

        //删除
        $scope.delete = function(x){
            //fetchFromStorage();
            var i;
            for (i = 0 ; i < arrayList.length; i++){
                if (arrayList[i].id == x.id){
                    break;
                }
            }

            if ( i < arrayList.length){
                arrayList.splice(i,1);
            }
            saveToStorage();
        }
    });
})();